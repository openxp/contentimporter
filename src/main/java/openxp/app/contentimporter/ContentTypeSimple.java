package openxp.app.contentimporter;


import java.util.LinkedHashSet;

public class ContentTypeSimple {

    LinkedHashSet<SimpleInput> inputs = new LinkedHashSet<SimpleInput>();

    public void add(SimpleInput simpleInput){
        inputs.add(simpleInput);
    }
    public LinkedHashSet<SimpleInput> getInputs() {
        return inputs;
    }

}
