package openxp.app.contentimporter;

import com.enonic.xp.form.*;
import com.enonic.xp.schema.mixin.Mixin;

import java.util.LinkedHashSet;

public class SimpleInput {
    LinkedHashSet<SimpleInput> children = new LinkedHashSet<>();

    public SimpleInput(Input input){
        this.name = input.getName();
        this.formType = input.getType().toString();
        this.path = input.getPath().getFirstElement();
        this.label = input.getLabel();
        this.type = input.getInputType().toString();
        Occurrences occurrences = input.getOccurrences();
        this.min = occurrences.getMinimum();
        this.max = occurrences.getMaximum();
        this.isMultiple = occurrences.isMultiple();
        this.impliesRequired = occurrences.impliesRequired();
        this.isUnlimited = occurrences.isUnlimited();
        this.elementCount = input.getPath().elementCount();
    }

    public SimpleInput(FormItemSet formItemSet){
        this.type = FormItemType.FORM_ITEM_SET.toString();
        this.name = formItemSet.getName();
        this.path = formItemSet.getPath().getFirstElement();
    }

    public SimpleInput(Mixin mixin){
        this.name = mixin.getName().getLocalName();
        this.type = FormItemType.MIXIN_REFERENCE.toString();
    }

    public SimpleInput(FieldSet fieldSet){
        this.name = fieldSet.getName();
        this.type = FormItemType.LAYOUT.toString();
    }

    String label;
    String name;
    String type;
    String formType;
    String path;
    String fullPath;
    int elementCount;
    int min;
    int max;
    boolean isMultiple;
    boolean impliesRequired;
    boolean isUnlimited;

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    public String getFullPath() {
        return fullPath!=null?fullPath:path;
    }

    public LinkedHashSet<SimpleInput> getChildren() {
        return children;
    }

    public boolean hasChildren() {
        return !getChildren().isEmpty();
    }
    public void setChildren(LinkedHashSet<SimpleInput> children) {
        this.children = children;
    }

    public void addSimpleInput(SimpleInput simpleInput) {
        this.children.add(simpleInput);
    }

    public void setFullPath(String fullPath){
        this.fullPath = fullPath;
    }
    public void setPath(String path){
        this.path = path;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getElementCount() {
        return elementCount;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public boolean isMultiple() {
        return isMultiple;
    }

    public boolean isImpliesRequired() {
        return impliesRequired;
    }

    public boolean isUnlimited() {
        return isUnlimited;
    }


}