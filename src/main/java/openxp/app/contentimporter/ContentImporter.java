package openxp.app.contentimporter;

import com.enonic.xp.content.*;
import com.enonic.xp.context.ContextAccessor;
import com.enonic.xp.context.ContextBuilder;
import com.enonic.xp.data.PropertyTree;
import com.enonic.xp.schema.content.ContentTypeName;
import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.enonic.xp.security.PrincipalKey;
import com.enonic.xp.security.RoleKeys;
import com.enonic.xp.security.User;
import com.enonic.xp.security.auth.AuthenticationInfo;
import com.enonic.xp.util.Reference;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

@Component(immediate = true)
public class ContentImporter implements ScriptBean {
    Supplier<ContentService> contentService;
    String displayName;
    ContentPath rootFolderPath;
    ContentTypeName targetContenttypeName;
    String[][] lines;
    List<String> headerColumns = new ArrayList<>();
    List<Map<String,String>> mappings = new ArrayList<>();

    Logger LOG = LoggerFactory.getLogger(ContentImporter.class);

    public void importParentChild() {
        PropertyTree data = new PropertyTree();
        data.addReference("category", Reference.from("0b27bd36-0560-4cf7-920b-25771ccf7cfb"));
        Map<String, ContentPath> namePath = new HashMap<>();


        runAs(RoleKeys.CONTENT_MANAGER_ADMIN, () -> {
            //ContentPath rootContentPath = ContentPath.from(rootFolderPath.toString());
            for (int i = 0; i < lines.length; i++) {
                String[] columns = lines[i];
                String thisParent = columns[0];
                String thisChild = columns[1];
                String thisChildDisplayName = columns[2];
                if (!namePath.containsKey(thisParent)){//Parent does not exist
                    CreateContentParams createContentParams = CreateContentParams.create()
                            .displayName(thisParent)
                            .name(ContentName.from(thisParent))
                            .parent(rootFolderPath)
                            .type(targetContenttypeName)
                            .requireValid(true)
                            .contentData(data)
                            .build();
                    contentService.get().create(createContentParams);
                    namePath.put(thisParent, ContentPath.from(rootFolderPath, thisParent));
                }
                if (namePath.containsKey(thisParent) && !namePath.containsKey(thisChild)){
                    //parent exist but child does not
                    ContentPath thisParentPath = namePath.get(thisParent);
                    CreateContentParams createContentParams = CreateContentParams.create()
                            .displayName(thisChildDisplayName)
                            .name(ContentName.from(thisChild))
                            .parent(thisParentPath)
                            .type(targetContenttypeName)
                            .requireValid(true)
                            .contentData(data)
                            .build();
                    contentService.get().create(createContentParams);
                    namePath.put(thisChild, ContentPath.from(thisParentPath, thisChild));
                }
            }
            return null;
        });
    }

    public void setLines(String[][] lines) {
        this.lines = lines;
    }

    public void setContentService(Supplier<ContentService> contentService) {
        this.contentService = contentService;
    }

    public void setHeaderColumns(List<String> headerColumns) {
        this.headerColumns = headerColumns;
    }

    public void setMapping(List<Map<String,String>> mappings) {
        this.mappings = mappings;
    }

    public void setTargetContenttype(String targetContenttype) {
        this.targetContenttypeName = ContentTypeName.from(targetContenttype);
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = ContentPath.from(rootFolderPath);
    }

    @Override
    public void initialize(final BeanContext context) {
        this.contentService = context.getService(ContentService.class);
    }

    private <T> T runAs(final PrincipalKey role, final Callable<T> runnable) {
        final AuthenticationInfo authInfo = AuthenticationInfo.create().principals(role).user(User.ANONYMOUS).build();
        return ContextBuilder.from(ContextAccessor.current()).authInfo(authInfo).build().callWith(runnable);
    }
}
