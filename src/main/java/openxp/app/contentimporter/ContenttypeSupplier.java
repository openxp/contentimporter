package openxp.app.contentimporter;

import com.enonic.xp.app.ApplicationKey;
import com.enonic.xp.form.*;
import com.enonic.xp.schema.content.*;
import com.enonic.xp.schema.mixin.Mixin;
import com.enonic.xp.schema.mixin.MixinService;
import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.google.common.collect.ImmutableSet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;

@Component(immediate = true)
public class ContenttypeSupplier implements ScriptBean {
    Supplier<ContentTypeService> contentTypeService;
    Supplier<MixinService> mixinService;
    Logger LOG = LoggerFactory.getLogger(this.getClass());

    public ContentTypes getContentTypes() {
        GetAllContentTypesParams getAllContentTypesParams = new GetAllContentTypesParams();
        ContentTypes contentTypes = contentTypeService.get().getAll(getAllContentTypesParams);
        return contentTypes;
    }

    public LinkedHashSet<SimpleInput> getContenttypeFormInputs(String appContentType) throws Exception {
        ContentTypeSimple contentTypeSimple = new ContentTypeSimple();

        if (appContentType == null || "".equals(appContentType)) {
            throw new Exception("contentType cannot be null or empty");
        }

        if (!appContentType.contains(":")) {
            throw new Exception("contentType is not on correct format applicationKey:contenttype");
        }

        String[] appContentTypeParts = appContentType.split(":");
        String guessedApplicationKey = appContentTypeParts[0];
        String guessedContenType = appContentTypeParts[1];

        ApplicationKey applicationKey = ApplicationKey.from(guessedApplicationKey);
        ContentTypes contentTypes = contentTypeService.get().getByApplication(applicationKey);
        Optional<ContentType> contentTypeOptional = contentTypes.stream().filter(contentType -> contentType.getName().getLocalName().equals(guessedContenType)).findAny();

        if (contentTypeOptional.isPresent()) {
            handleForm(contentTypeOptional.get().getForm(), contentTypeSimple, null);
        }
        return contentTypeSimple.getInputs();
    }

    private void handleForm(Form form, ContentTypeSimple contentTypeSimple, FormItem parentFormItem) {
        FormItems formItems = form.getFormItems();
        formItems.forEach(formItem -> handleFormItemType(formItem, contentTypeSimple, parentFormItem));
    }

    public void handleFormItemType(FormItem formItem, ContentTypeSimple contentTypeSimple, FormItem parentFormItem) {
        FormItemType formItemType = formItem.getType();

        if (FormItemType.LAYOUT == formItemType) {
            handleLayout((FieldSet) formItem, contentTypeSimple);
        } else if (FormItemType.FORM_ITEM_SET == formItemType) {
            handleFormItemSet((FormItemSet) formItem, contentTypeSimple);
        } else if (FormItemType.INPUT == formItemType) {
            SimpleInput simpleInput = new SimpleInput((Input) formItem);
            if (parentFormItem != null && parentFormItem.getName() != null && parentFormItem.getType() == FormItemType.FORM_ITEM_SET) {
                simpleInput.setFullPath(parentFormItem.getName() + "." + simpleInput.getName());
            }
            contentTypeSimple.add(simpleInput);
        } else if (FormItemType.MIXIN_REFERENCE == formItemType) {
            InlineMixin inlineMixin = (InlineMixin) formItem;
            Mixin mixin = mixinService.get().getByName(inlineMixin.getMixinName());
            handleForm(mixin.getForm(), contentTypeSimple, parentFormItem);
        }
    }

    public void handleLayout(FieldSet fieldSet, ContentTypeSimple contentTypeSimple) {
        FormItems formItems = fieldSet.getFormItems();
        formItems.forEach(formItem -> handleFormItemType(formItem, contentTypeSimple, fieldSet));
    }

    public void handleFormItemSet(FormItemSet formItemSet, ContentTypeSimple contentTypeSimple) {
        FormItems formItems = formItemSet.getFormItems();
        formItems.forEach(formItem -> handleFormItemType(formItem, contentTypeSimple, formItemSet));
    }

    @Override
    public void initialize(final BeanContext context) {
        this.contentTypeService = context.getService(ContentTypeService.class);
        this.mixinService = context.getService(MixinService.class);
    }
}
