var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contentimporter: require('/lib/contentimporter'),
    customselector: require('/lib/customselector')
};


exports.get = handleGet;

function handleGet(req) {

    var params = libs.customselector.parseParams(req.params);
    var body = libs.customselector.createResults(getItems(), params, false);

    return {
        contentType: 'application/json',
        body: body
    }
}

function getItems() {
    //Get content
    var content = libs.portal.getContent();

    if (content == undefined){
        return [{id:0,displayName:"Content is null",description:"",icon:null}];
    }

    var processedLines = libs.contentimporter.processLines(content);

    if (processedLines.lines == undefined || processedLines.lines.length < 1 || processedLines.headerColumns.length < 1){
        return [{id:0,displayName:'Cannot read lines in attached file',description:'',icon:null}];
    }

    var returnOptions = [];
    processedLines.headerColumns.forEach(function(element, index, array){
        var svgIcon = index;
        if (svgIcon > 9){
            svgIcon = 0;
        }
        returnOptions[index] = {
            id: element,
            displayName: element,
            description: element,
            iconUrl: libs.portal.assetUrl({path: 'img/'+svgIcon+'.svg'}),
        }
    });

    return returnOptions;
}