var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contenttypeSupplier: __.newBean("openxp.app.contentimporter.ContenttypeSupplier"),
    customselector: require('/lib/customselector')
};


exports.get = handleGet;

var contentTypes = libs.contenttypeSupplier.getContentTypes();

function handleGet(req) {
    var params = libs.customselector.parseParams(req.params);
    params.count = contentTypes.getSize();
    var body = libs.customselector.createResults(getItems(), params, true);

    return {
        contentType: 'application/json',
        body: body
    }
}

function getItems() {
    var returnOptions = [];
    contentTypes = libs.contenttypeSupplier.getContentTypes();
    contentTypes.forEach(function (contentType) {
        var name = contentType.name.toString();
        var displayName = contentType.displayName.toString();
        returnOptions.push({
            id: name,
            displayName: displayName,
            description: name,
            icon: null
        });

    });
    return returnOptions;
}

