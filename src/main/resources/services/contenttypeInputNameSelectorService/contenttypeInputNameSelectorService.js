var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contenttypeSupplier: __.newBean("openxp.app.contentimporter.ContenttypeSupplier"),
    customselector: require('/lib/customselector')
};


exports.get = handleGet;

function handleGet(req) {

    var params = libs.customselector.parseParams(req.params);
    var body = libs.customselector.createResults(getItems(), params, true);

    return {
        contentType: 'application/json',
        body: body
    }
}

function getItems() {
    //Get content
    var content = libs.portal.getContent();
    var targetContentType = content.data.targetContentType;
    if (targetContentType === undefined){
        return [{id:0,displayName:'You must select target contenttype first',description:'',icon:null}];
    }
    var contentTypeInputs = libs.contenttypeSupplier.getContenttypeFormInputs(targetContentType);

    var returnOptions = [];
    returnOptions.push({
        id: '_name',
        displayName: '_name',
        description: '_name',
        icon: null
    });
    returnOptions.push({
        id: '_displayName',
        displayName: '_displayName',
        description: '_displayName',
        icon: null
    });
    contentTypeInputs.forEach(function (contentTypeInput) {
        var id = contentTypeInput.fullPath;
        returnOptions.push({
            id: id,
            displayName: contentTypeInput.name,
            description: contentTypeInput.fullPath  + ' (' + contentTypeInput.type + ')',
            icon: null
        });
    });
    return returnOptions;
}