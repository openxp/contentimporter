(function (global) {

    global.ContentImporter = {
        container: document.getElementById('content-importer-widget-importpanel')
    };

    global.ContentImporter.container.addEventListener("click", doImport );

    function doImport(event){
        var componentUrl = event.target.dataset.componenturl;
        createRequest(componentUrl, handleResponse);
    }

    var createRequest = function(theUrl, callback)
    {
        console.log("create request to " + theUrl);
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                callback(xmlHttp.responseText);
        }
        xmlHttp.open("POST", theUrl, true); // true for asynchronous
        xmlHttp.send(null);
    };
    var handleResponse = function(responseText){
        console.log("response: " + responseText);
    };


}(window));