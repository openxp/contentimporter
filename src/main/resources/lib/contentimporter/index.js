var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    context: require('/lib/xp/context'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contentImporter: __.newBean("openxp.app.contentimporter.ContentImporter"),
};

function getSeparator(content) {
    return content.data.separator || ";";
};

function getHeaderLineIndex(content) {
    return content.data.headerLineIndex;//-1 means no header
};

function getMapping(content) {
    return content.data.mapping;
};

function getTargetContentType(content) {
    return content.data.targetContentType;
};

//TODO: Do some exception handling here
function processLines(content) {
    var attachmentStream = getAttachmentStream(content);
    var lines = [];
    var num = 0;
    libs.io.processLines(attachmentStream, function (line) {
        lines[num] = line.split(getSeparator(content));
        num++
    });

    var headerLineIndex = getHeaderLineIndex(content);
    return {
        //splice returns an array with 1 result, select first result.
        headerColumns: headerLineIndex !== -1 ? lines.splice(headerLineIndex, 1)[0] : [],
        lines: lines
    }
};

function doImport(content) {
    if (content.data.importFileType === "no-relationship") {
        doImportNoRelationshipFile(content);
    } else if (content.data.importFileType === "parent-child-relationship") {
        doImportParentChildFile(content)
    }
};

//TODO: Find out how to call context.run with content as parameter with returnValue
function createFolder() {
    var content = libs.portal.getContent();
    var folder = libs.content.create({
        parentPath: content._path,
        displayName: 'Import result',
        branch: 'draft',
        contentType: 'base:folder',
        data: content.data,
        requireValid: false
    });
    return folder;
}

function doCreateContent(targetFolder, content, column) {
    var mapping = getMapping(content);
    var targetContentType = getTargetContentType(content);

    var existingContent = libs.content.query({
        query: "displayName = '" + content + "'",
        count: 1, branch: 'draft',
        contentTypes: [targetContentType]
    });
    if (existingContent.hits.length > 0) {

    } else {
        libs.content.create({
            parentPath: targetFolder._path,
            displayName: column,
            branch: 'draft',
            requireValid: false,
            contentType: targetContentType,
            data: {}
        })
    }
}

function doImportParentChildFile(content) {
    var rootFolder = libs.context.run({principals: ['role:cms.admin']}, createFolder);
    var processedLines = processLines(content);
    libs.contentImporter.setRootFolderPath(rootFolder._path);
    libs.contentImporter.setTargetContenttype(getTargetContentType(content));
    libs.contentImporter.setLines(processedLines.lines);
    libs.contentImporter.setHeaderColumns(processedLines.headerColumns);
    libs.contentImporter.setMapping(getMapping(content));
    libs.contentImporter.importParentChild();
}


function doImportNoRelationshipFile(content) {
}


function getAttachmentStream(content) {
    var attachments = libs.util.data.forceArray(libs.content.getAttachments(content._path));
    var attachmentName = libs.jsonpath.process(attachments[0], '$..name');
    return libs.content.getAttachmentStream({key: content._id, name: attachmentName});
};

exports.doImport = doImport;
exports.processLines = processLines;
exports.getHeaderLineIndex = getHeaderLineIndex;
exports.getSeparator = getSeparator;