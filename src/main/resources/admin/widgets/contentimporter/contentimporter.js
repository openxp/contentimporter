var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf')
};

exports.get = function (req) {
    var uid = req.url.split('?uid=')[1];
    var view = resolve('contentimporter.html');
    var content = libs.portal.getContent();

    var params = {
        uid: uid,
        content: content
    };

    return {
        contentType: 'text/html',
        body: libs.thymeleaf.render(view, params)
    };
};