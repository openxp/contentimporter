var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf')
};

function handleGet(req) {

    var view = resolve('contentimporter.html');
    var content = libs.portal.getContent();
    var model = {
        mainRegion : content.page.regions['main']
    };

    return {
        contentType: 'text/html;charset=utf-8',
        body: libs.thymeleaf.render(view, model),
        pageContributions: {
            headEnd: [
                "<link href='"+libs.portal.assetUrl({path:'css/contentimporter.css'})+"' rel='stylesheet'/>"
            ]
        }
    };
}

exports.get = handleGet;
