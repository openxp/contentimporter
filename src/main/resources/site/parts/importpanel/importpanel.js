var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contentimporter: require('/lib/contentimporter')
};

function handleGet(req) {

    var content = libs.portal.getContent();
    var view = resolve('importpanel.html');
    var component = libs.portal.getComponent();
    var componentUrl = libs.portal.componentUrl({component: component.path});
    var model = {
        componentUrl: componentUrl+"?import=true"
    };

    return {
        contentType: 'text/html',
        body: libs.thymeleaf.render(view, model)
    };
}

function handlePost(req){
    if (req.params.import){
        var content = libs.portal.getContent();
        libs.contentimporter.doImport(content);
        return {body:"importing...",status:200}
    }
}

exports.get = handleGet;
exports.post = handlePost;
