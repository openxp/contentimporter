var libs = {
    content: require('/lib/xp/content'),
    portal: require('/lib/xp/portal'),
    thymeleaf: require('/lib/xp/thymeleaf'),
    io: require('/lib/xp/io'),
    util: require('/lib/enonic/util'),
    jsonpath: require('/lib/openxp/jsonpath'),
    contentimporter: require('/lib/contentimporter')
};

function handleGet(req) {

    var view = resolve('importfile.html');
    var content = libs.portal.getContent();

    var processedLines = libs.contentimporter.processLines(content);
    var model = {};

    model.lines = processedLines.lines;
    model.headerColumns = processedLines.headerColumns;
    model.content = content;

    return {
        contentType: 'text/html;charset=utf-8',
        body: libs.thymeleaf.render(view, model)
    };
}

exports.get = handleGet;
